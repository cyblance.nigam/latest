import React, { PropsWithChildren, useState } from "react";

import {
    Box,
    Button,
    ButtonProps,
    Dialog,
    DialogProps,
    IconButton,
    IconButtonProps,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/AddCircle";
import CloseIcon from "@material-ui/icons/Close";
import SearchIcon from "@material-ui/icons/Search";
import clsx from "clsx";

import AppBarHeader, { FilterOption } from "../../Layout/AppBarHeader";
import { IFilterProps } from "../../Models";
import SimpleMenu, { IMenuOption } from "../General/SimpleMenu";

import useStyles from "./Browser.styles";

export interface IBrowserProps<T> {
    options: IMenuOption<T>[];
    label?: string;
    buttonIcon?: React.ReactNode;
    dialog?: { title?: string; tall?: boolean; wide?: boolean };
    dialogProps?: DialogProps;
    buttonProps?: ButtonProps;
    skipMenu?: boolean;
    open?: boolean;
}
interface IProps<T> extends IBrowserProps<T> {
    onSearch?: (search: string) => void;
    filter?: IFilterProps;
    filterOptions?: FilterOption[];
    onChangeFilter?: (filter: IFilterProps) => void;
    onSelect: (values: T[], options?: IMenuOption<T>[]) => void;
    onClose: () => void;
}
const Browser = <T,>({
    options,
    label,
    buttonIcon,
    skipMenu,
    children,
    filter,
    filterOptions,
    onChangeFilter,
    onSearch,
    onSelect,
    dialog,
    dialogProps,
    buttonProps,
    open,
    onClose,
}: PropsWithChildren<IProps<T>>) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
    const doSkipMenu = skipMenu || options.length === 1;

    const onButtonClick = (anchor: HTMLElement | null) => {
        if (doSkipMenu) {
            onSelect(
                options.map(({ value }) => value),
                options
            );
        } else {
            setAnchorEl(anchor);
        }
    };
    const onMenuClick = (optionValue: T, option?: IMenuOption<T>) => {
        setAnchorEl(null);
        onSelect([optionValue], option ? [option] : undefined);
    };

    return (
        <>
            {label ? (
                <Button
                    color="secondary"
                    size="small"
                    onClick={(evt) => {
                        evt.stopPropagation();
                        onButtonClick(evt.currentTarget);
                    }}
                    startIcon={
                        buttonIcon ? (
                            buttonIcon
                        ) : doSkipMenu ? (
                            <SearchIcon />
                        ) : (
                            <AddIcon />
                        )
                    }
                    {...buttonProps}
                >
                    {label}
                </Button>
            ) : (
                <IconButton
                    color="secondary"
                    size="small"
                    onClick={(evt) => {
                        evt.stopPropagation();
                        onButtonClick(evt.currentTarget);
                    }}
                    {...(buttonProps as IconButtonProps)}
                >
                    {buttonIcon ? (
                        buttonIcon
                    ) : doSkipMenu ? (
                        <SearchIcon />
                    ) : (
                        <AddIcon />
                    )}
                </IconButton>
            )}

            <SimpleMenu<T>
                // header={label}
                options={options}
                anchorEl={anchorEl}
                onClick={onMenuClick}
                onClose={() => setAnchorEl(null)}
            />

            <Dialog
                {...dialogProps}
                open={Boolean(open)}
                onClose={onClose}
                maxWidth={false}
                onBackdropClick={undefined}
            >
                <AppBarHeader
                    title={dialog?.title || label}
                    onSearch={onSearch}
                    isDialog
                    filter={filter}
                    filterOptions={filterOptions}
                    onChangeFilter={onChangeFilter}
                >
                    <IconButton
                        edge="end"
                        color="inherit"
                        onClick={onClose}
                        aria-label="close"
                    >
                        <CloseIcon />
                    </IconButton>
                </AppBarHeader>
                <Box
                    className={clsx(classes.dialogContent, {
                        [classes.tallDialog]: dialog?.tall,
                        [classes.wideDialog]: dialog?.wide,
                    })}
                >
                    {open && children}
                </Box>
            </Dialog>
        </>
    );
};

export default Browser;
