import React from "react";

import AssignmentIcon from "@material-ui/icons/Assignment";
import WorkareaIcon from "@material-ui/icons/Bookmarks";
import EventNoteIcon from "@material-ui/icons/EventNote";
import ListIcon from "@material-ui/icons/ListAlt";
import TagIcon from "@material-ui/icons/LocalOffer";
import MenuBookIcon from "@material-ui/icons/MenuBook";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import CountryIcon from "@material-ui/icons/Public";

import { CollectionType } from "../../Config";
import { IMenuOption, MenuOptionDivider } from "../General/SimpleMenu";

export type CollectionBrowserMenu = IMenuOption<CollectionType[]>[];

export const AuthorPicker: CollectionBrowserMenu = [
    { value: ["author"], label: "Author", icon: <PersonAddIcon /> },
];

export const ArticlesPicker: CollectionBrowserMenu = [
    { value: ["articles"], label: "Articles", icon: <EventNoteIcon /> },
];

export const ContactsPicker: CollectionBrowserMenu = [
    { value: ["contacts"], label: "Contacts", icon: <PersonAddIcon /> },
];

export const CountryPicker: CollectionBrowserMenu = [
    { value: ["country"], label: "Country", icon: <CountryIcon /> },
    { value: ["region"], label: "Region", icon: <CountryIcon /> },
];

export const DossierPicker: CollectionBrowserMenu = [
    {
        value: ["dossier", "dossier_sub"],
        label: "Dossier",
        icon: <AssignmentIcon />,
    },
];

export const LibraryPicker: CollectionBrowserMenu = [
    {
        value: ["library"],
        label: "Library",
        icon: <MenuBookIcon />,
    },
];

export const WorkareaPicker: CollectionBrowserMenu = [
    {
        value: ["sdi_group", "workarea"],
        label: "Priorities",
        icon: <WorkareaIcon />,
    },
];

export const TagPicker: CollectionBrowserMenu = [
    { value: ["tag", "theme"], label: "Tag & Theme", icon: <TagIcon /> },
];

export const PersonsPicker: CollectionBrowserMenu = [
    { value: ["persons"], label: "Persons listings", icon: <WorkareaIcon /> },
];

export const StructurePicker: CollectionBrowserMenu = [
    {
        value: ["listing", "structure"],
        label: "Structural",
        icon: <ListIcon />,
    },
];

export const CollectionPicker: CollectionBrowserMenu = [
    ...StructurePicker,
    ...ArticlesPicker,
    MenuOptionDivider,
    ...LibraryPicker,
    MenuOptionDivider,
    ...DossierPicker,
    ...WorkareaPicker,
    MenuOptionDivider,
    ...AuthorPicker,
    ...ContactsPicker,
    ...PersonsPicker,
    MenuOptionDivider,
    ...CountryPicker,
    MenuOptionDivider,
    ...TagPicker,
];
