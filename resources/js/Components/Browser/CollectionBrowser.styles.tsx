import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    dialogContainer: {
        padding: theme.spacing(2),
    },
}));

export default useStyles;
