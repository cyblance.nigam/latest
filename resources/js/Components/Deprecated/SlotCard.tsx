import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import AddIcon from "@material-ui/icons/AddCircleOutline";
import EditIcon from "@material-ui/icons/EditOutlined";
import RemoveIcon from "@material-ui/icons/RemoveCircleOutline";
import cx from "clsx";
import route from "ziggy-js";

import Slot from "../../Models/Slot";

import ButtonConfirmDialog from "../General/ButtonConfirmDialog";
import { Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
    card: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
    },
    content: {
        padding: theme.spacing(1),
        paddingBottom: "0 !important",
        textAlign: "center",
    },
    actions: {
        display: "flex",
        justifyContent: "space-between",
        paddingTop: 0,
        opacity: 0.2,
    },
    hover: {
        opacity: 1,
    },
}));

interface IProps {
    slot: Slot;
    onChange?: (slot: Slot) => void;
}
const SlotCard: React.FC<IProps> = ({ slot: _slot, onChange }) => {
    const classes = useStyles();
    const [slot, setSlot] = useState(_slot);
    const [hover, setHover] = useState(false);

    const onAdd = () => {
        //
    };

    const onDetach = () => {
        setSlot((slot) => ({ ...slot, item_id: undefined }));
    };

    const onEdit = () => {
        if (!slot?.item_id) {
            return;
        }
        Inertia.get(
            route("admin.items.edit", { item: slot.item_id }).toString(),
            {}
        );
    };

    useEffect(() => {
        if (slot !== _slot) {
            onChange && onChange(slot);
        }
    }, [slot, _slot, onChange]);

    useEffect(() => {
        setSlot(_slot);
    }, [_slot]);

    return (
        <Card
            className={classes.card}
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
        >
            <CardContent className={classes.content}>
                <Typography variant="subtitle2">
                    {slot.title?.title ?? "?"}
                </Typography>
                <Typography variant="body2">
                    {slot.item_id ? "Has item" : <>{"No item"}</>}
                </Typography>
            </CardContent>
            <CardActions
                className={cx(classes.actions, hover && classes.hover)}
            >
                <ButtonConfirmDialog
                    buttonProps={{
                        size: "small",
                        disabled: !slot.item_id,
                    }}
                    icon={<RemoveIcon />}
                    onConfirm={onDetach}
                >
                    Detach this item.
                </ButtonConfirmDialog>
                <ButtonConfirmDialog
                    buttonProps={{
                        size: "small",
                        disabled: !slot.item_id,
                    }}
                    icon={<EditIcon />}
                    onConfirm={onEdit}
                >
                    Leave this screen to edit the slotted item, unsaved changes
                    will be lost.
                </ButtonConfirmDialog>
                <IconButton
                    color="secondary"
                    onClick={onAdd}
                    size="small"
                    disabled={!!slot.item_id}
                >
                    <AddIcon />
                </IconButton>
            </CardActions>
        </Card>
    );
};

export default SlotCard;
