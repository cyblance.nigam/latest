import React from "react";

import { Tooltip } from "@material-ui/core";
import PublicOffIcon from "@material-ui/icons/PublicOff";
import dayjs from "dayjs";

import { Item, Collection } from "../../Models";

import useStyles from "./PublishStatusIcon.styles";

interface IProps {
    item: Item | Collection;
}
const PublishStatusIcon: React.FC<IProps> = ({ item }) => {
    const classes = useStyles();
    const isPublished =
        item.status === "published" &&
        (!item.publish_at || dayjs(item.publish_at).isBefore(dayjs()));

    if (isPublished) {
        return <></>;
    }

    return (
        <Tooltip title="Unpublished">
            <PublicOffIcon
                fontSize="inherit"
                color="inherit"
                className={classes.icon}
            />
        </Tooltip>
    );
};

export default PublishStatusIcon;
