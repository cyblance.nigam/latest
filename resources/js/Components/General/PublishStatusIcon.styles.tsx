import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    icon: {
        fontSize: theme.typography.fontSize,
        color: theme.palette.grey["800"],
        marginLeft: theme.spacing(0.5),
    },
}));

export default useStyles;
