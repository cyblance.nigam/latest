import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles(() => ({
    input: {
        "& input": { opacity: 1.0 },
    },
}));

export default useStyles;
