import React, { useContext, useEffect, useState } from "react";

import { Box, Button } from "@material-ui/core";
import {
    GridApiContext,
    GridFooterContainerProps,
    selectedGridRowsCountSelector,
    useGridSelector,
} from "@material-ui/data-grid";

import { useListingContext } from "./ListingContext";
import useStyles from "./ListingSelectFooter.styles";

const ListingSelectFooter: React.FC<GridFooterContainerProps> = (props) => {
    const classes = useStyles();
    const { onCancel, onSelect } = useListingContext();
    const apiRef = useContext(GridApiContext);
    const options = useGridSelector(apiRef, (state) => state.options);
    const pagination = useGridSelector(apiRef, (state) => state.pagination);
    const selectedRowCount = useGridSelector(
        apiRef,
        selectedGridRowsCountSelector
    );
    const [enable, setEnable] = useState(false);

    const PaginationComponent =
        !!options.pagination &&
        pagination.pageSize != null &&
        !options.hideFooterPagination &&
        apiRef?.current.components.Pagination;

    const PaginationElement = PaginationComponent && (
        <PaginationComponent
            {...props}
            {...apiRef?.current.componentsProps?.pagination}
        />
    );

    const onClick = () => {
        const rows = apiRef?.current.getSelectedRows();
        if (rows?.size) {
            onSelect([...rows.values()][0]);
        }
    };

    useEffect(() => {
        setEnable(selectedRowCount > 0);
    }, [selectedRowCount]);

    return (
        <Box display="flex" justifyContent="space-between">
            {PaginationElement}
            <Box display="flex" alignItems="center">
                <Button
                    color="primary"
                    variant="text"
                    onClick={() => onCancel && onCancel()}
                    className={classes.button}
                >
                    Cancel
                </Button>
                <Button
                    color="primary"
                    disabled={!enable}
                    onClick={onClick}
                    variant="contained"
                    className={classes.button}
                >
                    Select
                </Button>
            </Box>
        </Box>
    );
};

export default ListingSelectFooter;
