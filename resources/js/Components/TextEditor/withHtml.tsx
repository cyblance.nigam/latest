import { parse } from "parse5";
import htmlparser2Adapter, { Element } from "parse5-htmlparser2-tree-adapter";
import { Editor, Transforms, Element as SlateElement, Path, Node } from "slate";

import { deserialize } from "./deserialize";

const withHtml = (editor: Editor) => {
    const { insertData, normalizeNode } = editor;

    editor.insertData = (data) => {
        const html = data.getData("text/html");

        if (html) {
            // const parsed = new DOMParser().parseFromString(html, "text/html");
            const parsed = parse(html, { treeAdapter: htmlparser2Adapter });
            const body = (parsed.firstChild as Element).lastChild as Element;
            const fragment = deserialize(body);
            console.log("paste fragment", html, body, fragment);
            Transforms.insertFragment(editor, fragment);
            return;
        }

        insertData(data);
    };

    editor.normalizeNode = ([node, path]) => {
        if (SlateElement.isElement(node)) {
            if (node.type === "paragraph") {
                // check for block elements as children
            }
            if (node.type === "list-item") {
                // has to be underneath a list, if not then unwrap it
                const parent = Node.parent(editor, path);
                if (
                    !SlateElement.isElement(parent) ||
                    (parent.type !== "ordered-list" &&
                        parent.type !== "unordered-list")
                ) {
                    Transforms.unwrapNodes(editor, { at: path });
                }
            }
            if (node.type === "caption" && Path.hasPrevious(path)) {
                // check whether previous node is also a caption,
                // if so transform this node to a paragraph
                const prevPath = Path.previous(path);
                const [prevNode] = Editor.node(editor, prevPath);
                if (
                    SlateElement.isElement(prevNode) &&
                    prevNode.type === "caption"
                ) {
                    // previous sibling is also a caption
                    Transforms.setNodes(
                        editor,
                        { type: "paragraph" },
                        { at: path }
                    );
                    Transforms.liftNodes(editor, { at: path, mode: "highest" });
                    return;
                }
            }
        }
        normalizeNode([node, path]);
    };

    return editor;
};

export default withHtml;
