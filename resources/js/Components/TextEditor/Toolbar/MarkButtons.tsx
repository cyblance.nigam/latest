import React from "react";

import { IconButton } from "@material-ui/core";
import FormatBoldIcon from "@material-ui/icons/FormatBold";
import FormatItalicIcon from "@material-ui/icons/FormatItalic";
import clsx from "clsx";
import { useSlate } from "slate-react";

import { isMarkActive, toggleMark } from "./MarkButtons.functions";
import useStyles from "./Toolbar.styles";

export type MarkElementTypes = "italic" | "bold" | "quote";

interface IMarkButtonProps {
    format: MarkElementTypes;
}
export const MarkButton: React.FC<IMarkButtonProps> = ({
    format,
    children,
}) => {
    const editor = useSlate();
    const classes = useStyles();
    const active = isMarkActive(editor, format);

    return (
        <IconButton
            color={active ? "primary" : undefined}
            className={clsx(classes.button, { [classes.buttonActive]: active })}
            onMouseDown={(event) => {
                event.preventDefault();
                toggleMark(editor, format);
            }}
            size="small"
        >
            {children}
        </IconButton>
    );
};

export const ItalicButton: React.FC = () => (
    <MarkButton format="italic">
        <FormatItalicIcon />
    </MarkButton>
);

export const BoldButton: React.FC = () => (
    <MarkButton format="bold">
        <FormatBoldIcon />
    </MarkButton>
);

export const QuoteButton: React.FC = () => (
    <MarkButton format="quote">&apos;</MarkButton>
);
