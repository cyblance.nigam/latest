import React from "react";

import { IconButton } from "@material-ui/core";
import RedoIcon from "@material-ui/icons/Redo";
import UndoIcon from "@material-ui/icons/Undo";
import { useSlateStatic } from "slate-react";

import useStyles from "./Toolbar.styles";

interface UndoProps {
    undo: boolean;
    redo?: never;
}
interface RedoProps {
    redo: boolean;
    undo?: never;
}
const HistoryButton: React.FC<UndoProps | RedoProps> = ({ undo = false }) => {
    const editor = useSlateStatic();
    const classes = useStyles();

    return (
        <IconButton
            className={classes.button}
            size="small"
            onMouseDown={(evt) => {
                evt.preventDefault();
                undo ? editor.undo() : editor.redo();
            }}
        >
            {undo ? <UndoIcon /> : <RedoIcon />}
        </IconButton>
    );
};

export default HistoryButton;
