import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import {
    Box,
    Button,
    Link,
    TextField,
    Tooltip,
    Typography,
} from "@material-ui/core";
import OpenInNewIcon from "@material-ui/icons/OpenInNew";

import ButtonConfirmDialog from "../../../General/ButtonConfirmDialog";
import { IPaneProps } from "../../Toolbar/ButtonDialog";

import useStyles from "./EditPane.styles";
import { FootnoteAttributes, FootnoteElementType, typeFootnote } from "./type";

// interface LinkAttributes extends Attributes {
//     url?: string;
// }

type IProps = IPaneProps<FootnoteElementType>;
const FootnoteEditPane: React.FC<IProps> = ({
    /*item,*/ elementIn,
    onChange,
}) => {
    const [attributes, setAttributes] = useState<FootnoteAttributes>({
        uuid: "",
    });
    const classes = useStyles();

    // useEffect(() => {}, [elementIn]);

    // useEffect(() => {}, [attributes]);

    useEffect(() => {
        onChange({
            ...attributes,
            type: typeFootnote,
            children: elementIn?.children || [
                { type: "paragraph", children: [{ text: "" }] },
            ],
        });
    }, [attributes, onChange, elementIn]);

    return <></>;
};

export default FootnoteEditPane;
