import React from "react";
import InsertPhotoIcon from "@material-ui/icons/InsertPhoto";
import ButtonDialog from "../../Toolbar/ButtonDialog";
import { ImageMenu } from "../../../Browser";
import ImagePane from "./EditPane";
import ItemElement from "./ItemElement";

const ImageToolbarButton: React.FC = () => {
    return (
        <ButtonDialog
            elementType={ItemElement}
            icon={<InsertPhotoIcon />}
            pickerMenu={ImageMenu}
            EditPane={ImagePane}
        />
    );
};

export default ImageToolbarButton;
