import { makeStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";
const useStyles = makeStyles((theme: Theme) => ({
    iframe: {
        maxWidth: 200,
        maxHeight: 200,
    },
}));

export default useStyles;
