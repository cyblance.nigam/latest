export const DCPROJECT_FIELDS = [
    "contact_person_name",
    "funding",
    "budget",
    "url",
];
export const DCPROJECT_HTMLFIELDS = [
    "description",
    "goals",
    "activity_type",
    "results",
];
