import React from "react";
import { Box, IconButton, Theme, Typography, Button } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import { makeStyles } from "@material-ui/styles";
import clsx from "clsx";
import { Attachment } from "../../Models/Attachment";
import { findTitle } from "../../Models/Content";
import { goEditItem } from "../../Utils/Links";
import ResourceDetail from "../Resources/ResourceDetail";
import { RendererProps } from "../Sorter";
import ButtonConfirmDialog from "../General/ButtonConfirmDialog";
import { useAppContext } from "../../Layout";

const useStyles = makeStyles((theme: Theme) => ({
    item: {
        display: "flex",
        width: "100%",
        alignItems: "center",
        borderBottomStyle: "solid",
        borderBottomWidth: 1,
        borderColor: theme.palette.divider,
        paddingBottom: theme.spacing(0.5),
        paddingTop: theme.spacing(0.5),
        "&:first-child": {
            borderTopWidth: 1,
            borderTopStyle: "solid",
        },
    },
    type: {
        flexBasis: "8%",
        flexShrink: 0,
    },
    title: {
        flexBasis: "25%",
        flexShrink: 0,
        flexGrow: 2,
        overflow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "pre",
    },
    nonResource: {
        // flexBasis: "70%",
    },
    detail: {
        flexBasis: "25%",
        flexShrink: 0,
        flexGrow: 2,
        overflow: "hidden",
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    smallIcon: { fontSize: "1rem" },
}));

export const AttachmentSorterRenderer: React.FC<RendererProps<Attachment>> = ({
    item: attachment,
    dragHandle,
    removeHandle,
    onSave12,
}) => {
    //console.log(onSave12);
    const classes = useStyles();
    const isResourceItem = attachment.item.type === "resource";
    const { needSave } = useAppContext();
    return (
        <div className={classes.item}>
            {dragHandle}
            <Typography
                variant="body1"
                className={clsx(
                    classes.title,
                    isResourceItem ? undefined : classes.nonResource
                )}
            >
                {findTitle(attachment.item) ??
                    attachment.item.contents[0].title ??
                    "-untitled-"}
            </Typography>
            <Box>
                {attachment.item.contents?.map(({ lang }) => lang).join(",")}
            </Box>
            {/* <IconButton
                size="small"
                onClick={() => {
                    goEditItem(attachment.item.id);
                }}
                color="secondary"
            >
                <EditIcon className={classes.smallIcon} />
            </IconButton>*/}
           
            <ButtonConfirmDialog
                buttonProps={{ size: "small" }}
                color="secondary"
                icon={<EditIcon />}
                onConfirm={() => {
                    goEditItem(attachment.item.id);
                }}
                needConfirmation={needSave}
            >
                {/* Added by Cyblance for Save content Data on edit Attachment */}
                Before proceeding further to Edit, Please click on "SAVE"  and then "Confirm" to prevent loss of updated data.
                
                <Button onClick={onSave12}>Save</Button>
            </ButtonConfirmDialog>
            {isResourceItem && (
                <Box className={classes.detail}>
                    <ResourceDetail item={attachment.item} />
                </Box>
            )}
            <Typography variant="caption" className={classes.type}>
                {attachment.item.subtype
                    ? attachment.item.subtype
                    : attachment.item.type}
            </Typography>
            {removeHandle}
        </div>
    );
};

export default AttachmentSorterRenderer;
