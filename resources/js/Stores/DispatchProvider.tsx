import React, { createContext, Dispatch, useContext } from "react";

import { CollectionAction, ItemAction } from "./actions";

const DispatchContext = createContext<
    Dispatch<ItemAction> | Dispatch<CollectionAction> | null
>(null);

interface IProps {
    dispatch: Dispatch<ItemAction> | Dispatch<CollectionAction>;
}
const DispatchProvider: React.FC<IProps> = ({ dispatch, children }) => (
    <DispatchContext.Provider value={dispatch}>
        {children}
    </DispatchContext.Provider>
);
export const useDispatch = () => useContext(DispatchContext)!;
export const useItemDispatch = () =>
    useContext(DispatchContext)! as Dispatch<ItemAction>;

export default DispatchProvider;
