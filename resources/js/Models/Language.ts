export interface Language {
    id: number;
    name: string;
    alias_name: string;
    is_active: "0" | "1";
    is_actives_subsite: "0" | "1";
    created_at: string;
    updated_at: string;
}
