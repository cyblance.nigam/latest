export interface Label {
    id: number;
    name: string;
    is_active: "active" | "in-active";
    created_at: string;
    updated_at: string;
}
