export interface Affiliate {
    acronym: string;
    official_name: string;
}
