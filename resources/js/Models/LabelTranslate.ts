export interface LabelTranslate {
    id: number;
    label_id: number;
    language_id: number;
    label_translate?: string;
    created_at: string;
    updated_at: string;
}
