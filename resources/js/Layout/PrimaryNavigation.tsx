import React from "react";

import { Inertia } from "@inertiajs/inertia";
import { usePage } from "@inertiajs/inertia-react";
import { Theme, Tooltip } from "@material-ui/core";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import AccountBoxIcon from "@material-ui/icons/AccountBox";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import AnnouncementIcon from "@material-ui/icons/Announcement";
import AssignmentIcon from "@material-ui/icons/Assignment";
import ContactPhoneIcon from "@material-ui/icons/ContactPhone";
import DescriptionIcon from "@material-ui/icons/Description";
import EventNoteIcon from "@material-ui/icons/EventNote";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import GroupWorkIcon from "@material-ui/icons/GroupWork";
import ImageIcon from "@material-ui/icons/Image";
import FileIcon from "@material-ui/icons/InsertDriveFile";
import LanguageIcon from "@material-ui/icons/Language";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import LinkIcon from "@material-ui/icons/Link";
import ListIcon from "@material-ui/icons/ListAlt";
import TagIcon from "@material-ui/icons/LocalOffer";
import LogoutIcon from "@material-ui/icons/Logout";
import ManageAccountsIcon from "@material-ui/icons/ManageAccounts";
import VideoIcon from "@material-ui/icons/OndemandVideo";
import PeopleIcon from "@material-ui/icons/People";
import PriorityHighIcon from "@material-ui/icons/PriorityHigh";
import CountryIcon from "@material-ui/icons/Public";
import TimelineIcon from "@material-ui/icons/Timeline";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
import { makeStyles } from "@material-ui/styles";
import { useLocalStorage } from "react-use";
import route from "ziggy-js";

/**  LanguagePageProps added by cyblance team **/
import { AuthPageProps, UserPageProps, Page, LanguagePageProps, LabelPageProps } from "../Models";
import LabelIcon from '@material-ui/icons/Label';
import GTranslateIcon from '@material-ui/icons/GTranslate';

const MENU_ITEMS = {
    items: [
        {
            filter: { type: "article" },
            label: "Articles",
            Icon: EventNoteIcon,
            tooltip: "News, opinion and blog series",
        },
        { filter: { type: "static" }, label: "Pages", Icon: DescriptionIcon },
        {
            filter: { type: "person,contact" },
            label: "Persons & Contacts",
            Icon: ContactPhoneIcon,
            tooltip: "Executive board, staff and office addresses",
        },
        {
            filter: { type: "dcproject" },
            label: "DC Projects",
            Icon: GroupWorkIcon,
            tooltip: "Development Cooperation projects",
        },
        {
            filter: { type: "library" },
            label: "Library documents",
            Icon: LibraryBooksIcon,
            tooltip: "Resolutions, constitution, policy briefs",
        },
        {
            filter: { type: "resource", subtype: "file" },
            label: "Files",
            Icon: FileIcon,
        },
        {
            filter: { type: "resource", subtype: "link" },
            label: "Links",
            Icon: LinkIcon,
        },
        {
            filter: { type: "resource", subtype: "video" },
            label: "Videos",
            Icon: VideoIcon,
        },
        {
            filter: {
                type: "resource",
                subtype: "image,image.portrait,image.icon,image.square",
            },
            label: "Images",
            Icon: ImageIcon,
        },
        { filter: {}, label: "All items" },
    ],
    collections: [
        {
            filter: { type: "articles" },
            label: "Article lists",
            Icon: AnnouncementIcon,
            tooltip: "News, opinion and blog series",
        },
        {
            filter: { type: "sdi_group,workarea" },
            label: "Priorities",
            Icon: PriorityHighIcon,
            tooltip: "Strategic Directions and work areas",
        },
        {
            filter: { type: "dossier,dossier_sub" },
            label: "Dossiers",
            Icon: AssignmentIcon,
            tooltip: "Spotlights and campaigns",
        },
        {
            filter: { type: "structure,listing" },
            label: "Structure",
            Icon: ListIcon,
            tooltip: "Structural collections",
        },
        {
            filter: { type: "library" },
            label: "Libraries",
            Icon: LibraryBooksIcon,
            tooltip: "Resolutions, constitution, policy briefs",
        },
        {
            filter: { type: "persons,contacts" },
            label: "Contact lists",
            Icon: ContactPhoneIcon,
            tooltip: "Executive board, staff and unit lists, offices",
        },
        // { filter: { type: "contacts" }, label: "Contacts", Icon: PeopleIcon },
        { filter: { type: "author" }, label: "Authors", Icon: PeopleIcon },
        { filter: { type: "country" }, label: "Countries", Icon: CountryIcon },
        {
            filter: { layout: "region" },
            label: "Regions",
            Icon: LanguageIcon,
        },
        {
            filter: { type: "tag,theme" },
            label: "Tags & Themes",
            Icon: TagIcon,
            tooltip: "Should not be used anymore",
        },
        { filter: {}, label: "All collections" },
    ],
};

const filterToQuery = (filter: { [key: string]: any }) => {
    const result: any = {};
    for (const key in filter) {
        result[`filter[${key}]`] = filter[key];
    }
    return result;
};

const useStyles = makeStyles((theme: Theme) => ({
    listNested: {
        paddingLeft: theme.spacing(4),
    },
}));

export const PrimaryNavigation: React.FC = () => {
    const classes = useStyles();
    const { can, user } = usePage<Page<AuthPageProps & UserPageProps>>().props;
    const [expandCollections, setExpandCollections] = useLocalStorage(
        "ei_expand_collections",
        true
    );
    const [expandItems, setExpandItems] = useLocalStorage(
        "ei_expand_items",
        true
    );
    const [expandUsers, setExpandUsers] = useLocalStorage(
        "ei_expand_users",
        false
    );
    // const { needSave } = useAppContext();
    const needSave = false;

    //Added By cyblance Team For language start
    const { canLang, language } = usePage<Page<AuthPageProps & LanguagePageProps>>().props;
    const [expandLanguages, setExpandLanguages] = useLocalStorage(
        "ei_expand_languages",
        false
    );

    const { canLabel, label } = usePage<Page<AuthPageProps & LabelPageProps>>().props;
    const [expandLabels, setExpandLabels] = useLocalStorage(
        "ei_expand_labels",
        false
    );

    //Added By cyblance Team For language end

    return (
        <List>
            <ListItem
                button
                onClick={() => Inertia.get(route("admin.dashboard"))}
            >
                <ListItemIcon>
                    <TimelineIcon />
                </ListItemIcon>
                <ListItemText>Dashboard</ListItemText>
            </ListItem>
            <ListItem button onClick={() => setExpandItems(!expandItems)}>
                <ListItemIcon>
                    <FileCopyIcon />
                </ListItemIcon>
                <ListItemText primary="Items" />
                {expandItems ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={expandItems} timeout="auto">
                <List>
                    <ListItem
                        button
                        disabled={needSave}
                        className={classes.listNested}
                        onClick={() => Inertia.get(route("admin.items.create"))}
                    >
                        <ListItemIcon>
                            <AddCircleIcon color="primary" />
                        </ListItemIcon>
                        <ListItemText primary="Create item" />
                    </ListItem>
                    {MENU_ITEMS.items.map(
                        ({ filter, label, Icon, tooltip }, idx) => (
                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(
                                        route(
                                            "admin.items.index",
                                            filterToQuery(filter)
                                        ).toString()
                                    )
                                }
                                className={classes.listNested}
                                key={idx}
                            >
                                <Tooltip title={tooltip || ""}>
                                    <ListItemIcon>
                                        {Icon ? <Icon /> : ""}
                                    </ListItemIcon>
                                </Tooltip>
                                <ListItemText primary={label} />
                            </ListItem>
                        )
                    )}{" "}
                </List>
            </Collapse>
            <ListItem
                button
                onClick={() => setExpandCollections(!expandCollections)}
            >
                <ListItemIcon>
                    <ViewModuleIcon />
                </ListItemIcon>
                <ListItemText primary="Collections" />
                {expandCollections ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={expandCollections} timeout="auto">
                <List>
                    <ListItem
                        button
                        disabled={needSave}
                        className={classes.listNested}
                        onClick={() =>
                            Inertia.get(route("admin.collections.create"))
                        }
                    >
                        <ListItemIcon>
                            <AddCircleIcon color="primary" />
                        </ListItemIcon>
                        <ListItemText primary="Create collection" />
                    </ListItem>
                    {MENU_ITEMS.collections.map(
                        ({ filter, label, Icon, tooltip }, idx) => (
                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(
                                        route(
                                            "admin.collections.index",
                                            filterToQuery(filter)
                                        ).toString()
                                    )
                                }
                                className={classes.listNested}
                                key={idx}
                            >
                                <ListItemIcon>
                                    <Tooltip title={tooltip || ""}>
                                        {Icon ? <Icon /> : <span></span>}
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary={label} />
                            </ListItem>
                        )
                    )}
                </List>
            </Collapse>
            {/* Added BY Cyblance Team start */}
            <ListItem button onClick={() => setExpandLanguages(!expandLanguages)}>
                <ListItemIcon>
                    <GTranslateIcon />
                </ListItemIcon>
                <ListItemText primary="Languages" />
                {expandLanguages ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={expandLanguages} timeout="auto">
                <List>
                    <ListItem
                        button
                        disabled={needSave}
                        onClick={() =>
                            Inertia.get(route("admin.languages.index"))
                        }
                        className={classes.listNested}
                    >
                        <ListItemIcon>
                            <Tooltip title={""}>
                            <GTranslateIcon />
                            </Tooltip>
                        </ListItemIcon>
                        <ListItemText primary="All languages" />
                    </ListItem>
                    {language && (
                        <>
                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(
                                        route("admin.languages.edit", { language })
                                    )
                                }
                                className={classes.listNested}
                            >
                                <ListItemIcon>
                                    <Tooltip title={""}>
                                        <ManageAccountsIcon />
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary="Edit Language" />
                            </ListItem>
                        </>
                    )}                    
                </List>
            </Collapse>
            <ListItem button onClick={() => setExpandLabels(!expandLabels)}>
                <ListItemIcon>
                    <LabelIcon />
                </ListItemIcon>
                <ListItemText primary="Labels" />
                {expandLabels ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={expandLabels} timeout="auto">
                <List>
                
                    <ListItem
                        button
                        className={classes.listNested}
                        onClick={() =>
                            Inertia.get(route("admin.labels.create"))
                        }
                    >
                    <ListItemIcon>
                            <AddCircleIcon
                                color={
                                    canLabel?.labe?.create
                                        ? "primary"
                                        : undefined
                                }
                            />
                        </ListItemIcon>
                        
                        <ListItemText primary="Add label" />
                    </ListItem>

                    <ListItem
                        button
                        disabled={needSave}
                        onClick={() =>
                            Inertia.get(route("admin.labels.index"))
                        }
                        className={classes.listNested}
                    >
                        <ListItemIcon>
                            <Tooltip title={""}>
                                <LabelIcon />
                            </Tooltip>
                        </ListItemIcon>
                        <ListItemText primary="All Labels" />
                    </ListItem>
                    {label && (
                        <>
                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(
                                        route("admin.labels.edit", { label })
                                    )
                                }
                                className={classes.listNested}
                            >
                                <ListItemIcon>
                                    <Tooltip title={""}>
                                        <ManageAccountsIcon />
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary="Edit Labels" />
                            </ListItem>
                        </>
                    )}
                </List>
            </Collapse>
            {/* Added BY Cyblance Team end */}
            <ListItem button onClick={() => setExpandUsers(!expandUsers)}>
                <ListItemIcon>
                    <AccountBoxIcon />
                </ListItemIcon>
                <ListItemText primary="Account" />
                {expandUsers ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={expandUsers} timeout="auto">
                <List>
                    {can?.users?.create && (
                        <>
                            <ListItem
                                button
                                disabled={needSave || !can?.users?.create}
                                className={classes.listNested}
                                onClick={() =>
                                    Inertia.get(route("admin.users.create"))
                                }
                            >
                                <ListItemIcon>
                                    <AddCircleIcon
                                        color={
                                            can?.users?.create
                                                ? "primary"
                                                : undefined
                                        }
                                    />
                                </ListItemIcon>
                                <ListItemText primary="Add user" />
                            </ListItem>

                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(route("admin.users.index"))
                                }
                                className={classes.listNested}
                            >
                                <ListItemIcon>
                                    <Tooltip title={""}>
                                        <PeopleIcon />
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary="All users" />
                            </ListItem>
                        </>
                    )}
                    {user && (
                        <>
                            <ListItem
                                button
                                disabled={needSave}
                                onClick={() =>
                                    Inertia.get(
                                        route("admin.users.edit", { user })
                                    )
                                }
                                className={classes.listNested}
                            >
                                <ListItemIcon>
                                    <Tooltip title={""}>
                                        <ManageAccountsIcon />
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary="My account" />
                            </ListItem>
                            <ListItem
                                button
                                onClick={() => Inertia.post(route("logout"))}
                                className={classes.listNested}
                            >
                                <ListItemIcon>
                                    <Tooltip title={""}>
                                        <LogoutIcon />
                                    </Tooltip>
                                </ListItemIcon>
                                <ListItemText primary="Logout" />
                            </ListItem>
                        </>
                    )}
                </List>
            </Collapse>
            
            
           
        </List>
    );
};

export default PrimaryNavigation;
