import React from "react";

import { createTheme, ThemeProvider } from "@material-ui/core/styles";

const theme = createTheme({
    components: {
        MuiFormControl: {
            defaultProps: {
                margin: "normal",
            },
        },
        MuiTextField: {
            defaultProps: {
                margin: "normal",
            },
        },
    },
    // palette: { text: { hint: "rgba(0, 0, 0, 0.38)" } },
});

export const Themed: React.FC = ({ children }) => {
    return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export default Themed;
