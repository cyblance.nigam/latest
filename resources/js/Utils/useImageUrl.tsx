import { useEffect, useState } from "react";

import { ImageResource } from "../Models/Resource";

export interface ImageProps {
    w?: number;
    h?: number;
    fit?: string;
}
const useImageUrl = (
    resource: ImageResource | undefined,
    imageProps?: ImageProps,
    azurepath?: string
): string | undefined => {
    const [url, setUrl] = useState<string>();

    useEffect(() => {
        // Azure Image Path by cyblance
        if(azurepath=='Yes'){
            setUrl(
                resource?.path
                    ? `https://eiwebsite.blob.core.windows.net/blockblobszjdmoh/${resource.path.replace("img/", "")}?w=${
                          imageProps?.w || ""
                      }&h=${imageProps?.h || ""}&fit=${imageProps?.fit || ""}`
                    : undefined
            );   
        }else{
            setUrl(
                resource?.path
                    ? `http://localhost/ei-latest/img/${resource.path.replace("img/", "")}?w=${
                          imageProps?.w || ""
                      }&h=${imageProps?.h || ""}&fit=${imageProps?.fit || ""}`
                    : undefined
            );
        }
       
    }, [resource, imageProps,azurepath]);

    return url;
};

export default useImageUrl;
