import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import {
    Box,
    Button,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Paper,
    Select,
    TextField,
    Typography,
    FormHelperText,
    Theme,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import route from "ziggy-js";

import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import { ErrorPageProps } from "../../Models";

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        paddingTop: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
    },
}));

interface IProps {
    is_actives: { [key: string]: string };
}
const Create: React.FC<IProps & ErrorPageProps> = ({ is_actives, errors }) => {
    const classes = useStyles();
    const [name, setName] = useState<string>("");
    const [is_active, setIsActives] = useState<string>("");
    const [isValid, setIsValid] = useState(false);

    const onCreate = () => {
        const params: any = { name,  is_active };
        Inertia.post(route("admin.labels.store").toString(), params);
    };

    useEffect(() => {
        if (Object.entries(is_actives).length === 1) {
            setIsActives(Object.entries(is_actives)[0][0]);
        }
    }, [is_actives]);

    useEffect(() => {
        setIsValid(Boolean(is_active) && Boolean(name));
    }, [is_active, name]);

    return (
        <>
            <AppBarHeader title="Create Label" />
            <ContentScroll>
                <Box className={classes.container}>
                    <Paper className={classes.container}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant="body1">
                                    Provide the required information. The
                                    password will be set by the user via a link
                                    sent to their email address.
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    label="Name"
                                    variant="outlined"
                                    fullWidth
                                    helperText={errors?.name}
                                />
                            </Grid>
                            
                            <Grid item xs={12}>
                                <FormControl variant="outlined" fullWidth>
                                    <InputLabel>Status</InputLabel>
                                    <Select
                                        value={is_active}
                                        onChange={(e) =>
                                            setIsActives(e.target.value as string)
                                        }
                                        label="Type"
                                        fullWidth
                                    >
                                        {Object.entries(is_actives).map(
                                            ([role, label], i) => (
                                                <MenuItem value={role} key={i}>
                                                    {label}
                                                </MenuItem>
                                            )
                                        )}
                                    </Select>
                                    <FormHelperText>
                                        {errors?.is_active}
                                    </FormHelperText>
                                </FormControl>
                            </Grid>

                            <Grid item xs={12}>
                                <Button
                                    size="large"
                                    variant="contained"
                                    color="primary"
                                    onClick={onCreate}
                                    disabled={!isValid}
                                >
                                    Create
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
            </ContentScroll>
        </>
    );
};

export default Create;
