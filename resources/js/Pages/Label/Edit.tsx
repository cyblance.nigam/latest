import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import {
    Box,
    Button,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EmailIcon from "@material-ui/icons/Email";
import route from "ziggy-js";

import ButtonConfirmDialog from "../../Components/General/ButtonConfirmDialog";
import Section from "../../Components/Section";
import { AppBarHeader, ContentScroll, useAppContext } from "../../Layout";
import { AuthPageProps, ErrorPageProps, Label } from "../../Models";

interface IProps extends AuthPageProps {
    labelModel: Label;
    is_actives: Record<string, string>;
}
const Edit: React.FC<IProps & ErrorPageProps> = ({
    labelModel: label,
    errors,
    is_actives,
    can,
}) => {
    const { needSave, setNeedSave } = useAppContext();

    const [name, setName] = useState(label.name);
    const [is_active, setIsActive] = useState(label.is_active); 

    useEffect(() => {
        setName(label.name);
        setIsActive(label.is_active);
    }, [label]);

    useEffect(() => {
        setNeedSave(
            name !== label.name ||  is_active !== label.is_active
        );
    }, [setNeedSave, label, name, is_active]);

    const onReset = () => {
        setName(label.name);
        setIsActive(label.is_active);
    };

    const onSave = () => {
        Inertia.patch(
            route("admin.labels.update", { label }).toString(),
            {  name, is_active },
            {
                preserveState: true,
            }
        );
        setNeedSave(false);
    };

    const onDelete = () => {
        Inertia.delete(route("admin.labels.destroy", { label }));
    };

    

    return (
        <>
            <AppBarHeader title="Edit Label">
                <Button
                    variant="outlined"
                    onClick={onReset}
                    color="secondary"
                    disabled={!needSave}
                >
                    Reset
                </Button>
                <Button
                    variant={needSave ? "contained" : "outlined"}
                    onClick={onSave}
                    color="secondary"
                    disabled={!needSave}
                >
                    Save
                </Button>
            </AppBarHeader>
            <ContentScroll>
                <Section title="Label details" open={true}>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                label="Name"
                                variant="outlined"
                                fullWidth
                                helperText={errors?.name}
                            />
                        </Grid>
                        {can?.is_active?.update && (
                            <Grid item xs={12}>
                                <FormControl variant="outlined" fullWidth>
                                    <InputLabel>is_active</InputLabel>
                                    <Select
                                        value={is_active}
                                        onChange={(e) =>
                                            setIsActive(e.target.value as any)
                                        }
                                        label="Type"
                                        fullWidth
                                    >
                                        {Object.entries(is_actives).map(
                                            ([is_active, label], i) => (
                                                <MenuItem value={is_active} key={i}>
                                                    {label}
                                                </MenuItem>
                                            )
                                        )}
                                    </Select>
                                    <FormHelperText>
                                        {errors?.is_active}
                                    </FormHelperText>
                                </FormControl>
                            </Grid>
                        )}
                    </Grid>
                </Section>
                <Section title="Actions">
                    <Grid container spacing={4} alignItems="baseline">
                        
                        {can?.label?.delete && (
                            <>
                                <Grid item xs={4}>
                                    <ButtonConfirmDialog
                                        color="secondary"
                                        label="Delete label"
                                        variant="contained"
                                        onConfirm={() => onDelete()}
                                        icon={<DeleteIcon />}
                                    >
                                        Deleting the user is irreversible.
                                    </ButtonConfirmDialog>
                                </Grid>
                            </>
                        )}
                    </Grid>
                </Section>
            </ContentScroll>
        </>
    );
};

export default Edit;
