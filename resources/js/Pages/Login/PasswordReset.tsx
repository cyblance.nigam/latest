import React, { KeyboardEventHandler, useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { IconButton } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import route from "ziggy-js";

import { FlashMessages } from "../../Layout";
import { ErrorPageProps } from "../../Models";

import useStyles from "./Login.styles";

interface IProps {
    email: string;
    token: string;
}
const PasswordReset: React.FC<IProps & ErrorPageProps> = ({
    email,
    token,
    errors,
}) => {
    const styles = useStyles();
    const [feedback, setFeedback] = useState<string>();
    const [password, setPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [passwordFocused, setPasswordFocused] = useState(false);
    const [passwordConfirmFocused, setPasswordConfirmFocused] = useState(false);

    useEffect(() => {
        if (errors?.error) {
            setFeedback(errors.error);
        }
    }, [errors]);

    const doReset = () => {
        Inertia.post(route("password.update"), {
            email,
            password,
            password_confirmation: passwordConfirm,
            token,
        });
    };

    const onKeyPress: KeyboardEventHandler<HTMLElement> = (e) => {
        if (password !== passwordConfirm) {
            return;
        }
        if (e.key === "Enter") {
            doReset();
        }
    };

    return (
        <form>
            <input
                type="hidden"
                id="email"
                autoComplete="username"
                value={email}
            />
            <FlashMessages />
            <Box className={styles.container}>
                <Paper className={styles.formbox}>
                    <Typography variant="h4">Update password</Typography>
                    <Typography variant="body1">
                        Provide a new password for account with email address:{" "}
                        {email}
                    </Typography>
                    <Typography variant="body1" className={styles.feedback}>
                        {feedback}
                    </Typography>
                    <TextField
                        label="New password"
                        value={password}
                        variant="outlined"
                        type={showPassword ? "text" : "password"}
                        className={styles.field}
                        onChange={(e) => setPassword(e.target.value)}
                        onKeyPress={onKeyPress}
                        helperText={
                            password.length < 12 ? "Minimum 12 characters" : " "
                        }
                        autoComplete="new-password"
                        id="new-password-text-field"
                        InputProps={{
                            endAdornment: (
                                <IconButton
                                    onClick={() =>
                                        setShowPassword((show) => !show)
                                    }
                                    size="small"
                                >
                                    {showPassword ? (
                                        <VisibilityOffIcon />
                                    ) : (
                                        <VisibilityIcon />
                                    )}
                                </IconButton>
                            ),
                        }}
                        error={
                            !passwordFocused &&
                            password.length > 0 &&
                            password.length < 12
                        }
                        onFocus={() => setPasswordFocused(true)}
                        onBlur={() => setPasswordFocused(false)}
                    />
                    <TextField
                        label="Confirm new password"
                        value={passwordConfirm}
                        variant="outlined"
                        type={showPassword ? "text" : "password"}
                        className={styles.field}
                        onChange={(e) => setPasswordConfirm(e.target.value)}
                        onKeyPress={onKeyPress}
                        helperText={
                            password === passwordConfirm
                                ? " "
                                : "Does not match"
                        }
                        autoComplete="new-password"
                        id="confirm-password-text-field"
                        InputProps={{
                            endAdornment: (
                                <IconButton
                                    onClick={() =>
                                        setShowPassword((show) => !show)
                                    }
                                    size="small"
                                >
                                    {showPassword ? (
                                        <VisibilityOffIcon />
                                    ) : (
                                        <VisibilityIcon />
                                    )}
                                </IconButton>
                            ),
                        }}
                        error={
                            !passwordConfirmFocused &&
                            passwordConfirm.length > 0 &&
                            password !== passwordConfirm
                        }
                        onFocus={() => setPasswordConfirmFocused(true)}
                        onBlur={() => setPasswordConfirmFocused(false)}
                    />
                    <Button
                        variant="contained"
                        size="large"
                        className={styles.button}
                        onClick={doReset}
                        disabled={!password || password !== passwordConfirm}
                    >
                        Update password
                    </Button>
                    <Button
                        variant="text"
                        onClick={() => Inertia.get(route("password.request"))}
                    >
                        Get new reset email
                    </Button>
                </Paper>
            </Box>
        </form>
    );
};

export default PasswordReset;
