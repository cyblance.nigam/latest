import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import { Box, IconButton } from "@material-ui/core";
// import Switch from "@material-ui/core/Switch";
import { GridColDef } from "@material-ui/data-grid";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles } from "@material-ui/styles";
import route from "ziggy-js";

import LinkQuery from "../../Components/General/LinkQuery";
import PublishStatusIcon from "../../Components/General/PublishStatusIcon";
import Listing, { DataCellCollectionTree } from "../../Components/Listing";
import useDataSource from "../../Components/useDataSource";
import { StatusType } from "../../Config";
import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import Collection from "../../Models/Collection";
import { findTitle } from "../../Models/Content";
import { AuthPageProps, IListingPageProps } from "../../Models/Inertia";
import Paginated from "../../Models/Paginated";
import { LangColumn, makeDateColumn } from "../../Utils/DataGridUtils";

//Added By Cyblance for delete functionality
import MenuItem from '@material-ui/core/MenuItem';
import ButtonConfirmDialog from "../../Components/General/ButtonConfirmDialog";
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';


// Added by cyblance For trnaslation functionality
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import GTranslateIcon from '@material-ui/icons/GTranslate';
import CircularProgress from '@material-ui/core/CircularProgress';

import ShareIcon from '@material-ui/icons/Share';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(() => ({
    cellRightAlign: {
        display: "block !important",
    },
    noLabel: {
        margin: 5,
    },
    noLabels:{
       
    },
}));

interface IProps extends IListingPageProps, AuthPageProps {
    collections: Paginated<Collection>;
    parent_types: string[];
    child_types: string[];
    isActiveLanguages: Record<string, string>;
    isActiveLanguagesss: Record<string, string>;
}
const List: React.FC<IProps> = ({
    collections,
    child_types,
    filter: _filter,
    sort,
    can,
    isActiveLanguages,
    isActiveLanguagesss,
}) => {
    const classes = useStyles();
    const [filter, setFilter] = useState(_filter);
    const [search, setSearch] = useState<string>();
    const dataSource = useDataSource({
        mode: "inertia",
        paginatedData: collections,
        search,
        filter,
        sort,
    });
    // const { parse, format, formats } = useUtils<AdapterDayjs>();
    // console.log(formats);

    // const [statuses, setStatuses] = useState<{
    //     [key: number]: StatusType;
    // }>({});

    useEffect(() => {
        const newStatuses: { [key: number]: StatusType } = {};
        collections.data.forEach(({ id, status }) => {
            newStatuses[id] = status;
        });
        // setStatuses(newStatuses);
    }, [collections]);

    // const toggleStatus = (id: number) => {
    //     setStatuses((statuses) => ({
    //         ...statuses,
    //         [id]: statuses[id] === "published" ? "unpublished" : "published",
    //     }));
    //     console.log("ToDo toggle publish", id);
    // };
    
    //Added By Cyblance for delete functionality
    const handleChangeDelCollection = () => {
        const getSessionData: any = sessionStorage.getItem("getId");
        const getData = JSON.parse(getSessionData);
        const count = getData.length;
        if (count == 0) {
            setState({ opens: true, vertical: 'top', horizontal: 'center' });
        } else {
            const getResult = getData.id;
            const getResultAction = actionTrash;
            Inertia.post(route("admin.collections.bulkDelete", { getResult, getResultAction }).toString(), {
            });
        }
    };

    const [state, setState] = React.useState({
        opens: false,
        vertical: 'top',
        horizontal: 'center',
    });

    const { vertical, horizontal, opens } = state;
    const handleCloses = () => {
        setState({ ...state, opens: false });
    };
    const [value, setValue] = React.useState(0);
    const handleChange = (newValue: any) => {
        console.log(value);
        console.log(newValue);
        setValue(newValue);
    };

    const [withoutTrash, setUnTrash] = React.useState('');
    const [withTrash, setTrash] = React.useState('');
    const [actionTrash, setActionTrash] = React.useState('');
    const handleTrash = (e: any) => {
        console.log(actionTrash)
        console.log(e.target.value)
        setActionTrash(e.target.value);
    }
    useEffect(() => {
        const getUrl = window.location.search;
        let currentUrl = window.location.href;
        var res = getUrl.substring(1, 7);
        if(res == 'filter'){
            let splitCurrentUrl = currentUrl.split('&');
            if (splitCurrentUrl[1] == 'trash=yes') {
                setValue(1);
            } else {
                setValue(0);
            }
            setUnTrash(splitCurrentUrl[0] + '&trash=no');
            setTrash(splitCurrentUrl[0] + '&trash=yes');
        }else{
            let splitCurrentUrl = currentUrl.split('?');
            if (splitCurrentUrl[1] == 'trash=yes') {
                    setValue(1);
            } else {
                    setValue(0);
            }
            setUnTrash(splitCurrentUrl[0] + '?trash=no');
            setTrash(splitCurrentUrl[0] + '?trash=yes');
        }
    });

    //Trnaslation functionality start
    const [name, setName] = useState(isActiveLanguages);
    const [names, setNames] = useState(isActiveLanguagesss);
    const [lang, setTranslation] = React.useState('');
    const handleSelectLang = (event: any) => {
        setTranslation(event.target.value);
    };

    const [DailogopenTranslate, setModelOpenTranslate] = React.useState(false);
    const [ids, setID] = React.useState('');
    let a = {};
    const handleModelClickOpenTranslate = (id: any) => {
      setID(id);
      setModelOpenTranslate(true);
      fetch('https://dev.ei-ie.org/EiiE-website-2021-latest/api/translate1').then(response =>
        a = response,
        setID1(a)
      );
    };
  
    const handleModelCloseTranslate = () => {
      setModelOpenTranslate(false);
    };
  
    const [ids1, setID1] = React.useState() as any;
    const isOpen = () => {
        fetch('https://dev.ei-ie.org/EiiE-website-2021-latest/api/translate1').then(response =>
        console.log(response.json())
        );
    }

    //end code select box for language translation
    const [a11, setLoader] = React.useState(false);
    const onSaveTranslation = () => {
        setLoader(true);
        Inertia.get('https://dev.ei-ie.org/EiiE-website-2021-latest/admin/translate-collection/' + lang + '/' + ids, {
            preserveState: true,
            onSuccess: ()=> {
               setLoader(false);
            }
        }); 
    };
    useEffect(() => {
        setName(isActiveLanguages);
        setNames(isActiveLanguagesss);
      }, [name]);
    //Translation functionality end  

    //Share
    const [myArray, setMyArray] = useState([]) as any ;
    const [Dailogopen, setModelOpen] = React.useState(false);
    const [shareids, setshareids] = React.useState('');
    const [langs, setlangs] = React.useState('');
    const handleModelClickOpen = (id: any) => {
      setshareids(id);
      setModelOpen(true);
      setMyArray({...myArray, ['type'] : 'collection',['id'] : id });
    };
    const handleModelClickClose = () => {
        setModelOpen(false);
    };
    
    const handleChangeLanguage = (event: any) => {
        setlangs(event.target.value);
        setMyArray({...myArray, ['language'] : event.target.value });
        //setMyArray({...myArray, ['type'] : 'collection' });
      };
     //Sechulde Date popup get
     const [sech, setSech] = React.useState('');
     const handleChangeSech = (event: React.ChangeEvent<HTMLInputElement>) => {
         setSech((event.target as HTMLInputElement).value);
         if(event.target.value=='sechulde'){
             setShow(!show)
             setMyArray({...myArray, ['sech'] : event.target.value });
         }else{
             setShow(show)
             setMyArray({...myArray, ['sech'] : event.target.value });
         }  
     };
     //Share Icons of facebook get data by Cyblance
     function setfacebook(id:any){
       setMyArray({...myArray, ['social'] : 'fb' });
     };
     //Share Icons of Twitter get data by Cyblance
     function setTwitter(id:any){
       setMyArray({...myArray, ['social'] : 'tw' });
     };
     //Share Icons of Linkdin get data by Cyblance
     function setLinkden(id:any){
       setMyArray({...myArray, ['social'] : 'ld' });    
     };
    const [show, setShow] = useState(false);
    const setField = (key: string, value: any) => {
        setMyArray({...myArray, ['sech_date'] : value });
    };
    const onSave = () => {
      setModelOpen(false); 
      Inertia.post(route("admin.share"),{myArray}, {
          preserveState: true,
          onSuccess(info){ 
            setModelOpen(false);
          }
      });
    };
    const handleModelClose = () => {
        setModelOpen(false);
      };
    //share

    const onCreate = () => {
        Inertia.get(route("admin.collections.create"));
    };

    useEffect(() => {
        setFilter(_filter);
    }, [_filter]);

    const columns: GridColDef[] = [
        {
            field: "id",
            renderCell: (cell) => (
                <Box display="flex" justifyContent="space-between">
                    {cell.id}{" "}
                    <DataCellCollectionTree
                        collection={cell.row as Collection}
                        onSelect={(subCollection) =>
                            Inertia.get(
                                route("admin.collections.edit", {
                                    id: subCollection.id,
                                })
                            )
                        }
                    />
                </Box>
            ),
            align: "right",
        },
        {
            //Added field for translation by Cyblance
            field: "translate",
            sortable: false,
            renderCell: (cell) => (
              <div>
                <IconButton onClick={() => handleModelClickOpenTranslate(cell.row.id)}  >
                  <GTranslateIcon />
                </IconButton>
              </div>
            ),
            flex: 1,
        },
        {
            //Added field for sharepost by Cyblance
            field:"Share Post",
            sortable: false,
            renderCell:(cell)=>(
                <div>
                  <IconButton onClick={() => handleModelClickOpen(cell.row.id)} >
                    <ShareIcon/>
                  </IconButton>
                </div>   
              ),
              flex: 2,
        },
        /*
        {
            field: "status",
            headerName: "Published",
            disableClickEventBubbling: true,
            renderCell: (cell) => (
                <Switch
                    checked={
                        statuses[(cell.row as Collection).id] === "published"
                    }
                    color="primary"
                    onChange={() => {
                        toggleStatus((cell.row as Collection).id);
                    }}
                />
            ),
        },
        */
        {
            field: "title",
            renderCell: (cell) => (
                <>
                    {child_types?.includes((cell.row as Collection).type) ? (
                        <>&mdash;&nbsp;&nbsp;</>
                    ) : (
                        <></>
                    )}
                    <InertiaLink
                        href={route("admin.collections.edit", {
                            id: cell.row.id,
                        }).toString()}
                    >
                        {<PublishStatusIcon item={cell.row as Collection} />}{" "}
                        {findTitle(cell.row as Collection) ?? "- no title -"}
                    </InertiaLink>
                </>
            ),
            flex: 2,
        },
        LangColumn,
        makeDateColumn("created_at"),
        makeDateColumn("updated_at"),
        {
            field: "items_count",
            headerName: "Items",
            renderCell: (cell) => (
                <LinkQuery
                    toRoute="admin.items.index"
                    query={{ filter: { ["collection.id"]: cell.row.id } }}
                >
                    {cell.value}
                </LinkQuery>
            ),
            cellClassName: () => classes.cellRightAlign,
            align: "right",
        },
        { field: "type" },
        { field: "layout" },
    ];

    const filterOptions = [
        {
            label: "Missing translations",
            name: "missing_translations",
        },
        {
            label: "Empty collections",
            name: "items_count",
            value: "0",
        },
        {
            label: "Hide unpublished",
            name: "status",
            value: "published",
        },
    ];
    if (filter?.filter?.type?.includes("dossier")) {
        (filterOptions as any).push({
            label: "Show subdossiers",
            name: "type",
            value: "dossier,dossier_sub",
            disabledValue: "dossier",
        });
    }

    return (
        <>
            <AppBarHeader
                title="Collections"
                filterOptions={filterOptions}
                filter={filter}
                onSearch={setSearch}
                onChangeFilter={setFilter}
            >
                <IconButton
                    onClick={onCreate}
                    disabled={
                        !can?.collections?.create &&
                        !can?.collections?.createLimited
                    }
                >
                    <AddIcon />
                </IconButton>
            </AppBarHeader>
            <ContentScroll>
               
                {/*Added By Cyblance for delete functionality*/}
                <div>
                    <Box component="span" sx={{ p: 2 }}>
                    {
                        value == 0 ?
                        <FormControl variant="outlined" style={{width:'20%'}}  margin="dense" >
                            <InputLabel id="demo-simple-select-outlined-label">Bulk Action</InputLabel>
                            <Select
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                onChange={handleTrash}
                                label="Bulk Action"
                            >
                            <MenuItem value="trashed">Move to Trash</MenuItem>
                            </Select>
                        </FormControl>:
                        <FormControl variant="outlined" style={{width:'20%'}}  margin="dense" >
                            <InputLabel id="demo-simple-select-outlined-label">Bulk Action</InputLabel>
                            <Select
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                onChange={handleTrash}
                                label="Bulk Action"
                                >
                                <MenuItem value="restored">Restore</MenuItem>
                                <MenuItem value="deleted">Delete permanently</MenuItem>
                            </Select>
                        </FormControl>
                    }
                    </Box>
               
                    <FormControl variant="outlined" style={{width:'10%'}} margin="dense">
                        <ButtonConfirmDialog
                            color="primary"
                            label="Apply"
                            variant="contained"
                            onConfirm={() => handleChangeDelCollection()}
                        >
                        Deleting the Colletion is irreversible.
                        </ButtonConfirmDialog>
                    </FormControl>
                </div>
                <Snackbar
                    color="primary"
                    anchorOrigin={{ vertical: "top", horizontal: "center" }}
                    open={opens}
                    autoHideDuration={6000}
                    message="Please Select The Collection First"
                    key={vertical + horizontal}
                    action={
                        <React.Fragment>
                            <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloses}>
                                <CloseIcon fontSize="small" />
                            </IconButton>
                        </React.Fragment>
                    }
                />
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={handleChange}
                    aria-label="disabled tabs example"
                >
                    <InertiaLink
                        href={withoutTrash}>
                        <Tab label="All" />
                    </InertiaLink>
                    <InertiaLink
                        href={withTrash}>
                        <Tab label="Trash" />
                    </InertiaLink>
                </Tabs>
                <Listing columns={columns} dataSource={dataSource}></Listing>
               
            </ContentScroll>
            <Dialog open={Dailogopen} onClose={handleModelClickClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Share</DialogTitle>
                <DialogContent>
                    <FormControl style={{width:'100%'}}>
                    <InputLabel id="demo-simple-select-outlined-label">Languages *</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select"
                        value={langs}
                        onChange={handleChangeLanguage}
                        label="Languages"
                        >
                        {Object.entries(name).map(
                            ([is_active, label123], i) => (
                            <MenuItem value={is_active}>{label123}</MenuItem>
                            )
                        )}
                        </Select>
                    </FormControl>
                    <FormLabel style={{width:'100%'}} > Social Media *</FormLabel>  
                    
                    <IconButton color="primary" onClick={()=>setfacebook(shareids)}  >
                        <FacebookIcon/>
                    </IconButton>
                    <IconButton color="primary" onClick={()=>setTwitter(shareids)} >
                        <TwitterIcon/>
                    </IconButton>
                    <IconButton color="primary" onClick={()=>setLinkden(shareids)}>
                        <LinkedInIcon/>
                    </IconButton>
            
                    <FormControl style={{width:'100%'}}>
                        <FormLabel style={{width:'100%'}} > Share  *</FormLabel>
                        <RadioGroup aria-label="Share"  name="share" value={sech} onChange={handleChangeSech}>
                        <FormControlLabel value="share"  control={<Radio color="primary" />} label="Share" />
                        <FormControlLabel value="sechulde"  control={<Radio color="primary" />} label="sechulde" />
                        </RadioGroup>
                        <div>
                            {show ? (
                                <TextField 
                                    type="datetime-local"
                                    onChange={(e) => setField("date",e.target.value)} 
                                />
                            ) : null}
                        
                        </div>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleModelClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onSave} color="primary">
                        Share
                    </Button>
                </DialogActions>
            </Dialog>
            {/* Translation Dialog Box Added By Cyblance start  */}
            <Dialog open={DailogopenTranslate} onClose={handleModelCloseTranslate} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Translate</DialogTitle>
                    <DialogContent>
                    <DialogContentText>
                        Please select language 
                    </DialogContentText>

                    <FormControl style={{width:'100%'}}>
                        <InputLabel id="demo-simple-select-outlined-label">Translate</InputLabel> 
                        <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={lang}
                            onChange={handleSelectLang}
                            label="translate"
                        >
                            {Object.entries(name).map(
                            ([is_active, label123], i) => (
                                <MenuItem value={is_active}>{label123}</MenuItem>
                            )
                            )}
                        </Select>
                    </FormControl>
                    </DialogContent>
                    {a11 ? '' :
                    <DialogActions>
                        <Button onClick={handleModelCloseTranslate} color="primary">
                        Cancel
                        </Button>
                        <Button onClick={onSaveTranslation} color="primary">
                        Translate
                        </Button>
                    </DialogActions>
                    }
                    {a11 ? <Box component="span" sx={{ p: 2 }}>
                    <CircularProgress />
                    </Box> : ""}
            </Dialog> 
            {/* Translation Dialog Box Added By Cyblance end */}
        </>
    );
};

export default List;
