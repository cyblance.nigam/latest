import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import { Tooltip } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import { GridColDef } from "@material-ui/data-grid";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import route from "ziggy-js";
import PublishStatusIcon from "../../Components/General/PublishStatusIcon";
import Listing from "../../Components/Listing";
import useDataSource from "../../Components/useDataSource";
import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import { Collection } from "../../Models";
import { findTitle } from "../../Models/Content";
import { AuthPageProps, IListingPageProps } from "../../Models/Inertia";
import Item from "../../Models/Item";
import Paginated from "../../Models/Paginated";
import { LangColumn, makeDateColumn } from "../../Utils/DataGridUtils";

//Added By Cyblance for delete functionality
import ButtonConfirmDialog from "../../Components/General/ButtonConfirmDialog";
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import MenuItem from '@material-ui/core/MenuItem';

// Added by cyblance For trnaslation functionality
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import GTranslateIcon from '@material-ui/icons/GTranslate';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';

// Add By cybalnce For shareing 
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel'
import ShareIcon from '@material-ui/icons/Share';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TextField from '@material-ui/core/TextField';

interface IProps extends IListingPageProps, AuthPageProps {
    items: Paginated<Item>;
    collection?: Collection;
    isActiveLanguages: Record<string, string>;
    isActiveLanguagesss: Record<string, string>;
}
const List: React.FC<IProps> = ({
    items,
    collection,
    filter: _filter,
    sort,
    can,
    isActiveLanguages,
    isActiveLanguagesss,
}) => {
    const [search, setSearch] = useState<string>();
    const [filter, setFilter] = useState(_filter);
    const dataSource = useDataSource({
        mode: "inertia",
        paginatedData: items,
        search,
        filter,
        sort,
    });

    const columns: GridColDef[] = [
        {
            field: "id",
            renderCell: (cell) => (
                <>
                    {cell.row.id} <PublishStatusIcon item={cell.row as Item} />
                </>
            ),
        },
        {
            //Added field for sharepost by Cyblance
            field:"Share Post",
            sortable: false,
            renderCell:(cell)=>(
                <div>
                  <IconButton onClick={() => handleModelClickOpen(cell.row.id)} >
                    <ShareIcon/>
                  </IconButton>
                </div>   
            ),
              flex: 2,
        },
        {
            //Added field for translation by Cyblance
            field: "translate",
            sortable: false,
            renderCell: (cell) => (
              <div>
                <IconButton onClick={() => handleModelClickOpenTranslate(cell.row.id)}  >
                  <GTranslateIcon />
                </IconButton>
              </div>
            ),
            flex: 1,
        },
        {
            field: "title",
            renderCell: (cell) => (
                <InertiaLink
                    href={route("admin.items.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {findTitle(cell.row as Item) ||
                        (cell.row as Item).contents[0]?.title ||
                        "- no title -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        LangColumn,
        makeDateColumn("created_at"),
        makeDateColumn("updated_at"),
        { field: "type" },
        { field: "subtype",
        sortable: false, },
    ];

    //Added By Cyblance for delete functionality
    const [actionTrash, setActionTrash] = React.useState('');
    const handleTrash = (e: any) => {
        setActionTrash(e.target.value);
    }
  
    const handleChangeDel = () => {
        const getSessionData: any = sessionStorage.getItem("getId");
        const getData = JSON.parse(getSessionData);
        const count = getData.length;
        if (count == 0) {
        setState({ opens: true, vertical: 'top', horizontal: 'center' });
        } else {
        const getResult = getData.id;
        const getResultAction = actionTrash;
        Inertia.post(route("admin.items.bulkDelete", { getResult, getResultAction }).toString(), {
        });
        }
    };
    const [value, setValue] = React.useState(0);

    const handleChange = (newValue: any) => {
        setValue(newValue);
    };
    const [state, setState] = React.useState({
        opens: false,
        vertical: 'top',
        horizontal: 'center',
      });
    
    const { vertical, horizontal, opens } = state;
      const handleCloses = () => {
        setState({ ...state, opens: false });
      };
    const [withoutTrash, setUnTrash] = React.useState('');
    const [withTrash, setTrash] = React.useState('');
    
    //Trnaslation functionality start
    const [name, setName] = useState(isActiveLanguages);
    const [names, setNames] = useState(isActiveLanguagesss);
    const [lang, setTranslation] = React.useState('');
    const handleSelectLang = (event: any) => {
        setTranslation(event.target.value);
    };

    const [DailogopenTranslate, setModelOpenTranslate] = React.useState(false);
    const [ids, setID] = React.useState('');
    let a = {};
    const handleModelClickOpenTranslate = (id: any) => {
      setID(id);
      setModelOpenTranslate(true);
      fetch('https://dev.ei-ie.org/EiiE-website-2021-latest/api/translate1').then(response =>
        a = response,
        setID1(a)
      );
    };
  
    const handleModelCloseTranslate = () => {
      setModelOpenTranslate(false);
    };
  
    const [ids1, setID1] = React.useState() as any;
    const isOpen = () => {
        fetch('https://dev.ei-ie.org/EiiE-website-2021-latest/api/translate1').then(response =>
        console.log(response.json())
        );
    }

    //end code select box for language translation
    const [a11, setLoader] = React.useState(false);
    const onSaveTranslation = () => {
        setLoader(true);
        Inertia.get('https://dev.ei-ie.org/EiiE-website-2021-latest/admin/translate/' + lang + '/' + ids, {
            preserveState: true,
            onSuccess: () => {
               setLoader(false);

            }
        }); 
    };
    useEffect(() => {
        setName(isActiveLanguages);
        setNames(isActiveLanguagesss);
      }, [name]);
    //Trnaslation functionality end
    // Sahreing functionlaity Start
    const [myArray, setMyArray] = useState([]) as any ;
    const [Dailogopen, setModelOpen] = React.useState(false);
    const [shareids, setshareids] = React.useState('');
    const [langs, setlangs] = React.useState('');
    const handleModelClickOpen = (id: any) => {
        console.log(id);
      setshareids(id);
      setModelOpen(true);
      setMyArray({...myArray, ['type'] : 'item',['id'] : id });
    };
  
    const handleModelClickClose = () => {
        setModelOpen(false);
    };

    const handleChangeLanguage = (event: any) => {
      setlangs(event.target.value);
      setMyArray({...myArray, ['language'] : event.target.value });
    };
    
    //Sechulde Date  popup get
    const [sech, setSech] = React.useState('');
    const handleChangeSech = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSech((event.target as HTMLInputElement).value);
        if(event.target.value=='sechulde'){
            setShow(!show)
            setMyArray({...myArray, ['sech'] : event.target.value });
        }else{
            setShow(show)
            setMyArray({...myArray, ['sech'] : event.target.value });
        }  
    };
    //Share Icons of facebook get data by Cyblance
    function setfacebook(id:any){
      setMyArray({...myArray, ['social'] : 'fb'});
    };
    //Share Icons of Twitter get data by Cyblance
    function setTwitter(id:any){
      setMyArray({...myArray, ['social'] : 'tw' });
    };
    //Share Icons of Linkdin get data by Cyblance
    function setLinkden(id:any){
      setMyArray({...myArray, ['social'] : 'ld' });    
    };
    const [show, setShow] = useState(false);
    const setField = (key: string, value: any) => {
        setMyArray({...myArray, ['sech_date'] : value });
    };
    const onSave = () => {
      setModelOpen(false); 
      Inertia.post(route("admin.share"),{myArray}, {
          preserveState: true,
          onSuccess(info){ 
            setModelOpen(false);
          }
      });
      
    };
    const handleModelClose = () => {
        setModelOpen(false);
      }
    // shareing functionlaity end 
    useEffect(() => {
        const getUrl = window.location.search;
        let currentUrl = window.location.href;
        var res = getUrl.substring(1, 7);
        if(res == 'filter'){
        let splitCurrentUrl = currentUrl.split('&');
        if (splitCurrentUrl[1] == 'trash=yes') {
            setValue(1);
        } else {
            setValue(0);
        }
        setUnTrash(splitCurrentUrl[0] + '&trash=no');
        setTrash(splitCurrentUrl[0] + '&trash=yes');
        }else{
        let splitCurrentUrl = currentUrl.split('?');
        if (splitCurrentUrl[1] == 'trash=yes') {
                setValue(1);
        } else {
                setValue(0);
        }
        setUnTrash(splitCurrentUrl[0] + '?trash=no');
        setTrash(splitCurrentUrl[0] + '?trash=yes');
        }
    });
    const onCreate = () => {
        Inertia.get(route("admin.items.create"));
    };

    useEffect(() => {
        setFilter(_filter);
    }, [_filter]);

    /*const [Msg, setMsg] = useState('');
    if(actionTrash=='trashed'){
        setMsg('abc');
    }else if(actionTrash=='restored'){
        setMsg('123');
    }else{
        setMsg('555');
    }*/

    const title = collection ? `Items in '${findTitle(collection)}'` : `Items`;

    return (
        <>
            <AppBarHeader
                title={title}
                afterTitle={
                    collection?.id ? (
                        <Tooltip title="Edit collection">
                            <IconButton
                                size="small"
                                onClick={() =>
                                    Inertia.get(
                                        route("admin.collections.edit", {
                                            id: collection.id,
                                        })
                                    )
                                }
                                color="inherit"
                            >
                                <EditIcon />
                            </IconButton>
                        </Tooltip>
                    ) : undefined
                }
                filterOptions={[
                    {
                        label: "Missing translations",
                        name: "missing_translations",
                    },
                    {
                        label: "Missing workarea",
                        name: "missing_workarea",
                    },
                    // ...(filter?.filter.subtype === "file" ||
                    // filter?.filter.subtype?.startsWith("image")
                    //     ? [
                    //           {
                    //               label: "Missing files",
                    //               name: "missing_files",
                    //           },
                    //       ]
                    //     : []),
                    {
                        label: "Hide unpublished",
                        name: "status",
                        value: "published",
                    },
                ]}
                filter={filter}
                onSearch={setSearch}
                onChangeFilter={setFilter}
            >
                <IconButton
                    onClick={onCreate}
                    disabled={can ? !can?.items?.create : true}
                >
                    <AddIcon />
                </IconButton>
            </AppBarHeader>
            <ContentScroll>
                {/*Added By Cyblance for delete functionality*/}
            <div>
                <Box component="span" sx={{ p: 2 }}>
                {
                    value == 0 ?
                    <FormControl variant="outlined" style={{width:'20%'}} margin="dense">
                        <InputLabel id="demo-simple-select-outlined-label">Bulk Action</InputLabel>
                        <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            onChange={handleTrash}
                            label="Bulk Action"
                            >
                            <MenuItem value="trashed">Move to Trash</MenuItem>
                        </Select>
                    </FormControl>:
                    <FormControl variant="outlined" style={{width:'20%',margin:"dense"}}>
                        <InputLabel id="demo-simple-select-outlined-label">Bulk Action</InputLabel>
                        <Select
                            labelId="demo-simple-select-outlined-label"
                            onChange={handleTrash}
                            label="Bulk Action"
                        >
                        <MenuItem value="restored">Restore</MenuItem>
                        <MenuItem value="deleted">Delete permanently</MenuItem>
                        </Select>
                    </FormControl>
                }
                </Box>
                {console.log(actionTrash)}
                <FormControl variant="outlined" style={{width:'10%'}}  margin="dense">
                    <ButtonConfirmDialog
                    color="primary"
                    label="Apply"
                    variant="contained"
                    onConfirm={() => handleChangeDel()}
                    >
                        Deleting the Item is irreversible.
                    </ButtonConfirmDialog>
                </FormControl>
            </div>
            <Snackbar
                color="primary"
                anchorOrigin={{ vertical: "top", horizontal: "center" }}
                open={opens}
                autoHideDuration={6000}
                message="Please Select The Item First"
                key={vertical + horizontal}
                action={
                    <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloses}>
                        <CloseIcon fontSize="small" />
                    </IconButton>
                    </React.Fragment>
                }
                />
                 <Tabs
                    value={value}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={handleChange}
                    aria-label="disabled tabs example"
                >
                    <InertiaLink
                    href={withoutTrash}>
                    <Tab label="All" />
                    </InertiaLink>
                    <InertiaLink
                    href={withTrash}>
                    <Tab label="Trash" />
                    </InertiaLink>
                </Tabs>
                <Listing columns={columns} dataSource={dataSource}></Listing>
                {/* Translation Dialog Box Added By Cyblance start  */}
                <Dialog open={DailogopenTranslate} onClose={handleModelCloseTranslate} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Translate</DialogTitle>
                    <DialogContent>
                    <DialogContentText>
                        Please select language
                    </DialogContentText>

                    <FormControl style={{width:'100%'}}>
                        <InputLabel id="demo-simple-select-outlined-label">Translate</InputLabel> 
                        <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={lang}
                            onChange={handleSelectLang}
                            label="translate"
                        >
                            {Object.entries(name).map(
                            ([is_active, label123], i) => (
                                <MenuItem value={is_active}>{label123}</MenuItem>
                            )
                            )}
                        </Select>
                    </FormControl>
                    </DialogContent>
                    {a11 ? '' :
                    <DialogActions>
                        <Button onClick={handleModelCloseTranslate} color="primary">
                        Cancel
                        </Button>
                        <Button onClick={onSaveTranslation} color="primary">
                        Translate
                        </Button>
                    </DialogActions>
                    }
                    {a11 ? <Box component="span" sx={{ p: 2 }}>
                    <CircularProgress /> 
                    </Box> : ""}
                </Dialog> 
                {/* Translation Dialog Box Added By Cyblance end */}
                {/* Share Daliog Box Added by Cybalnce start */}
                <Dialog open={Dailogopen} onClose={handleModelClickClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Share</DialogTitle>
                <DialogContent>
                    <FormControl style={{width:'100%'}}>
                    <InputLabel id="demo-simple-select-outlined-label">Languages *</InputLabel>
                        <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select"
                        value={langs}
                        onChange={handleChangeLanguage}
                        label="Languages"
                        >
                        {Object.entries(name).map(
                            ([is_active, label123], i) => (
                            <MenuItem value={is_active}>{label123}</MenuItem>
                            )
                        )}
                        </Select>
                    </FormControl>
                    <FormLabel style={{width:'100%'}} > Social Media *</FormLabel>  
                    
                    <IconButton color="primary" onClick={()=>setfacebook(shareids)}  >
                        <FacebookIcon/>
                    </IconButton>
                    <IconButton color="primary" onClick={()=>setTwitter(shareids)} >
                        <TwitterIcon/>
                    </IconButton>
                    <IconButton color="primary" onClick={()=>setLinkden(shareids)}>
                        <LinkedInIcon/>
                    </IconButton>
            
                    <FormControl style={{width:'100%'}}>
                        <FormLabel style={{width:'100%'}} > Share  *</FormLabel>
                        <RadioGroup aria-label="Share"  name="share" value={sech} onChange={handleChangeSech}>
                        <FormControlLabel value="share"  control={<Radio color="primary" />} label="Share" />
                        <FormControlLabel value="sechulde"  control={<Radio color="primary" />} label="sechulde" />
                        </RadioGroup>
                        <div>
                            {show ? (
                                <TextField 
                                    type="datetime-local"
                                    onChange={(e) => setField("date",e.target.value)} 
                                />
                            ) : null}
                        
                        </div>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleModelClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onSave} color="primary">
                        Share
                    </Button>
                </DialogActions>
                </Dialog>
                {/* Share Daliog Box Added by Cybalnce end */}   
            </ContentScroll>
        </>
    );
};

export default List;
