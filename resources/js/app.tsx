import React from "react";

import { createInertiaApp, PageResolver } from "@inertiajs/inertia-react";
import { InertiaProgress } from "@inertiajs/progress";
import { render } from "react-dom";

import { AdminLayout } from "./Layout";
import Themed from "./Layout/Themed";

InertiaProgress.init();

createInertiaApp({
    resolve: (name) =>
        import(`./Pages/${name}`).then(({ default: page }) => {
            if (page.layout === undefined) {
                if (name.startsWith("Login")) {
                    page.layout = (page: any) => <Themed>{page}</Themed>;
                } else {
                    page.layout = (page: any) => (
                        <Themed>
                            <AdminLayout>{page}</AdminLayout>
                        </Themed>
                    );
                }
            }
            return page;
        }) as unknown as PageResolver,
    setup({ el, App, props }) {
        render(<App {...props} />, el);
    },
});

// render(
//     <InertiaApp
//         initialPage={
//             app && app.dataset.page ? JSON.parse(app.dataset.page) : "{}"
//         }
//         resolveComponent={(name) =>
//             import(`./Pages/${name}`).then(({ default: page }) => {
//                 if (page.layout === undefined) {
//                     if (name.startsWith("Login")) {
//                         page.layout = (page: any) => <Themed>{page}</Themed>;
//                     } else {
//                         page.layout = (page: any) => (
//                             <Themed>
//                                 <AdminLayout>{page}</AdminLayout>
//                             </Themed>
//                         );
//                     }
//                 }
//                 return page;
//             })
//         }
//     />,
//     app
// );
