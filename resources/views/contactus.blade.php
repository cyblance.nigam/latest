@extends('main')

@section('title', $labelsArr['Education International'])

@section('head')
    @parent 
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        function onSubmit(token) {
            //Added validation by Cyblance
            var email=document.getElementById("email").value;
            var message=document.getElementById("message").value;
            if(email==''){
                document.getElementById("email").focus();
                document.getElementById('err_email').innerHTML='Email Field is Required';               
                return false ;
            }else if(message==''){
                document.getElementById("message").focus();
                document.getElementById('err_message').innerHTML='Message Field is Required'; 
                return false;
            }else{
                document.getElementById("contact-form").submit();
            }
        }
    </script>
    <style type="text/css">
    .grecaptcha-badge { 
        visibility: hidden; 
    }
    </style>
@endsection 

@section('content')
<div class="no_lead_image"></div>

<article class="article_main ">
    <header>
        <h2>{{ $labelsArr['Contact Us'] }}</h2>
    </header>

    <main>
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        <form id="contact-form" method="POST" action="{{ route('contact.form-post') }}">
            @csrf
            <label>
                <span>{{ $labelsArr['First name'] }}</span>
                <input type="text" name="first_name" value="{{ old('first_name') }}" />
            </label>

            <label>
                <span>{{ $labelsArr['Last name'] }}</span>
                <input type="text" name="last_name" value="{{ old('last_name') }}" />
            </label>

            <label>
                <!--Added * by cyblance to show mendetory fields-->
                <span>{{ $labelsArr['Email'] }}*</span>
                <input type="text" id="email" name="email" value="{{ old('email') }}" />
                <div id="err_email"></div>
                @error('email')
                    <div class="_error">
                        <div class="_error-arrow"></div>
                        <div class="_error-inner">{{ $errors->first('email') }}</div>
                    </div>
                @enderror
            </label>

            <label>
                <span>{{ $labelsArr['Phone number'] }}</span>
                <input type="text" name="phone" value="{{ old('phone') }}" />
            </label>

            {{-- <label>
                <span>{{ $labelsArr['Addressee'] }}</span>
                <select>
                    <optgroup label="EI Headquarters">
                        <option>Unit A</option>
                        <option>Unit B</option>
                    </optgroup>
                    <optgroup label="Region">
                        <option>ETUCE</option>
                        <option>ASIA</option>
                    </optgroup>
                </select>
            </label> --}}

            <label>
                <span>{{ $labelsArr['Topic'] }}</span>
                <input type="text" name="subject" value="{{ old('subject') }}" />
            </label>

            <label>
                 <!--Added * by cyblance to show mendetory fields-->
                <span>{{ $labelsArr['Message'] }}*</span>
                <div id="err_message"></div>
                @error('message')
                    <div class="_error">
                        <div class="_error-arrow"></div>
                        <div class="_error-inner">{{ $errors->first('message') }}</div>
                    </div>
                @enderror
                <textarea id="message" name="message">{{ old('message') }}</textarea>
            </label>

            <button 
                class="g-recaptcha" 
                data-sitekey="6LdW3rYaAAAAAO5KCFJw5XTwvg7v6Hgmf-QOXXVP" 
                data-callback="onSubmit"
            >{{ $labelsArr['Submit'] }}</button>

            <div class="recaptcha-branding">
                {!! $labelsArr['recaptcha_attrib'] !!}
            </div>
        </form>
    </main>
    @isset($offices[0])
        <section class="contact_us_offices affiliates-listing">        
            <header>
                <h3>Offices</h3>
            </header>
            @foreach ($offices as $office)
                @include('shared.card', ['item' => $office])
                {{--
                <article class="contact_us_office card card_affiliate">
                    <h4>{{ $office->content->title }}</h4>
                    @isset ($office->content->contact)
                        @include('shared.contact', ['contact' => $office->content->contact])
                    @endisset
                </article>
                --}}
            @endforeach
        </section>
    @endisset
</article>

@endsection 
