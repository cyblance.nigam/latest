<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsletterController extends Controller
{

	public function __construct(){
        
        $labelsArr = labels();
        $this->data['labelsArr'] = $labelsArr;
        view()->share('labelsArr', $labelsArr);
    }
	
    public function show() {
        return view()->first([
            'newsletter.activecampaign_'.\App::getLocale(),
            'newsletter.activecampaign_en'
        ]);
    }

}