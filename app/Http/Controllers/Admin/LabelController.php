<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use App\Models\Label;
use App\Models\LabelTranslate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\AllowedInclude;
use App\Filters\SearchLabelFilter;

class LabelController extends Controller
{
    public function index(Request $request)
    {   
        $labels = QueryBuilder::for(Label::class)
            ->allowedFilters([
               AllowedFilter::custom('search', new SearchLabelFilter),
            ])
            ->allowedSorts([
                'id',
                'created_at', 
                'updated_at', 
                'name', 
                'is_active',
            ])
            ->defaultSort('name')
            ->withoutGlobalScopes()
            ->paginate(16)
            ;
            
        $result = [
            'labels' => $labels,
            'filter' => $request->all('filter'),
            'sort' => $request->get('sort'),
        ];
        return Inertia::render('Label/List', $result);
    }


    public function edit(Request $request, Label $label) {
    
        $data = [
            'labelModel' => $label,
            'can' => [
                'is_active' => ['update' => 'updateIs_active', $label],
                'label' => ['delete' => 'delete', $label],
            ],
            'is_actives' => ['active' => 'active', 'in-active' => 'in-active'],
        ];
        $sharedCan = Inertia::getShared('can');
        if ($sharedCan) {
            $data['can'] = array_merge_recursive($sharedCan, $data['can']);
        }
        return Inertia::render('Label/Edit', $data);
    }

    public function create(Request $request) {
       
        return Inertia::render('Label/Create', ['is_actives' => ['active' => 'active', 'in-active' => 'in-active']]);
    }

    public function store(Request $request) {

        $request->validate([
            'name' => 'required',
            'is_active' => 'required',
        ]);
         
        $pass=Str::random(12);
        $label = new Label();
        $label->name=$request->name;
        $label->is_active=$request->is_active;
        $label->save();

        $labelTranslate = new LabelTranslate();
        $labelTranslate->label_id=$label->id;
        $labelTranslate->language_id=1;
        $labelTranslate->label_translate=$request->name;
        $labelTranslate->save();
        return Redirect::route('admin.labels.edit', $label)->with(['info' => 'Label created']);
    }


    public function update(Request $request, Label $label) {

        $request->validate([
            'name' => 'string|filled',
            'is_active' => 'string|filled',
        ]);

        if ($request->has('name')) {
            $label->name = $request->input('name');
        }
        if ($request->has('is_active')) {
            $label->is_active = $request->input('is_active');
        }
        $label->save();
        return Redirect::route('admin.labels.edit', $label)->with(['info' => 'Label updated']);
    }
}

