<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use App\Models\User;
use App\Models\LabelTranslation;
use App\Models\Label;
use App\Models\LabelTranslate;
use App\Models\Language;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\AllowedInclude;

class LabelTranslateController extends Controller
{

    public function edit(Request $request, LabelTranslate $labelTranslate) {

        $labelTranslateData = LabelTranslate::select('label_translates.*','languages.name')
        ->join('labels', 'label_translates.label_id', '=', 'labels.id')
        ->join('languages', 'languages.id', '=', 'label_translates.language_id')
        ->where('label_id',$labelTranslate->id)
        ->get();

        $labelTranslateData1 = Language::select('languages.name')
        ->where('languages.is_active','active')
        ->get();

        $data1 = [];
        foreach($labelTranslateData1 as $key => $value){

            if(isset($labelTranslateData[$key])){
                $data1[$value->name] =  $labelTranslateData[$key]->label_translate;
            }
            else{
                $data1[$value->name] = '';
            }
        }
    
        $data = [
            'labelTranslateModel' => $labelTranslate,
            'canlabelTranslate' => [
                'labelTranslate' => ['delete' => 'delete', $labelTranslate],
            ],
            'is_actives123' => $data1,
        ];

        $sharedCan = Inertia::getShared('canlabelTranslate');
        if ($sharedCan) {
            $data['canlabelTranslate'] = array_merge_recursive($sharedCan, $data['canlabelTranslate']);
        }

        return Inertia::render('LabelTranslate/Edit', $data);
    }

    public function update(Request $request, LabelTranslate $labelTranslate) {

        $request->validate([
            'label_translate' => 'string|filled',
        ]);

        $languages = Language::select('id','name')->where('is_active','active')->get();
        $data3 = [];
        foreach($languages as $key => $value){

            $data3[$value->name] =  $value->id;
        }
        $input = $request->all();

        $dataFinal = array();
        foreach($input['is_actives123'] as $key => $value){
           
            $dataFinal[$data3[$key]] = $value;
        }

        foreach($dataFinal as $key => $value){

            $result = DB::table('label_translates')->where('label_id', $labelTranslate->id)
            ->where('language_id', $key)->first();

            if($result){
                DB::table('label_translates')
                ->where('label_id', $labelTranslate->id)
                ->where('language_id', $key)
                ->update(array('label_translate' => $value));
            }else{
                if($value!=''){
                    $labelTranslate123 = new LabelTranslate();
                    $labelTranslate123->label_id=$labelTranslate->id;
                    $labelTranslate123->language_id=$key;
                    $labelTranslate123->label_translate=$value;
                    $labelTranslate123->save();
                }
            }
        }
        return Redirect::route('admin.labelTranslates.edit', $labelTranslate)->with(['info' => 'Lable translate updated']);
    }

    // Automated Labels Translator
    public function all_label_translate(Request $request, $lang_id) {
        $language=Language::find($lang_id); 
        //echo "<pre>"; print_r($request->all()); exit();
      
        if($language){
         
            //Data which is trnaslated
            $total_label_trnalated=LabelTranslate::select('*')
            ->join('labels', 'label_translates.label_id', '=', 'labels.id')
            ->where('language_id',$lang_id)
            ->where('label_translates.translate','Yes')
            ->where('labels.is_active','=','active');

            $total_label_trnalated_count=$total_label_trnalated->count();
            $total_label_trnalated=$total_label_trnalated->get();

            // Original Label Which in english             
            $labelsQuery = LabelTranslate::join('labels', 'label_translates.label_id', '=', 'labels.id')
            ->join('languages', 'languages.id', '=', 'label_translates.language_id')
            ->where('language_id',1)
            ->where('labels.is_active','=','active');

            // Flag Logic  
            if($total_label_trnalated_count==0){
                $labels =$labelsQuery->get();
            }else{
                foreach($total_label_trnalated as $labelTranslateData){
                    $ids[]=$labelTranslateData->label_id;    
                }
                $labels = $labelsQuery->whereNotIn('label_translates.id',$ids)->get();
            }
            // Trnaslation save save Data
            foreach($labels as $label){
                $get_labels=array();
                $get_labels[]=$label->label_translate;
                $translated_label = active_language_translate_tag($language->alias_name,$get_labels);
                $allLabelTranslate = LabelTranslate::create(['label_id' => $label->label_id,'language_id' => $lang_id,'label_translate' => $translated_label[0], 'translate' => 'Yes']);
            }
        }
        return Redirect::back()->with(['info' => 'Lable translate Done']);
    }
    public function label_check($id){
        $translate=LabelTranslate::where('language_id','=',$id)->where('translate','Yes')->count();
        $total_labels = LabelTranslate::join('labels', 'label_translates.label_id', '=', 'labels.id')
            ->join('languages', 'languages.id', '=', 'label_translates.language_id')
            ->where('labels.is_active','=','active')
            ->where('label_translates.language_id',1)
            ->count();
        $countLabels = $translate / $total_labels;
        $totalCount = $countLabels * 100;
        $count = number_format($totalCount, 2);
        $update=Language::where('id',$id)->update(['label_translate_per'=>$count]);
        return  $count;  
    }

}

