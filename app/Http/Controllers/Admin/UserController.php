<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Illuminate\Support\Facades\Password;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\AllowedInclude;
//Mail to send user when register start
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Filters\SearchUserFilter;
//Mail to send user when register End 

class UserController extends Controller
{
    public function index(Request $request)
    {
        $isInertia = $request->input('_format') != 'json';
        $input = $request->all();
        if ($request->user()->cannot('viewAny', User::class)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }
        $users = QueryBuilder::for(User::class)
            ->allowedFilters([
                // Added By Cyblance For Id Searching and role 
                AllowedFilter::custom('search', new SearchUserFilter),
            ])
            ->allowedSorts([
                'id',
                'created_at', 
                'updated_at', 
                'name', 
                'email', 
                'role',
            ])
            ->defaultSort('name')
            ->withoutGlobalScopes();
        
            if(isset($input['trash']) && $input['trash']=='yes'){
                $users = $users->onlyTrashed();
            }else{
                $users = $users->where('deleted_at',null);
            }
            if ($isInertia) {
                $users = $users->paginate(16)->appends(request()->query());
            } else {
                $users = $users->jsonPaginate()->appends(request()->query());
            }    
        
        $result = [
            'users' => $users,
            'filter' => $request->all('filter'),
            'sort' => $request->get('sort'),
        ];
        return Inertia::render('Users/List', $result);
    }


    public function edit(Request $request, User $user) {
        if ($request->user()->cannot('update', $user)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }
    
        $data = [
            'userModel' => $user,
            'can' => [
                'role' => ['update' => $request->user()->can('updateRole', $user)],
                'user' => ['delete' => $request->user()->can('delete', $user)],
            ],
            'roles' => ['editor' => 'Editor', 'admin' => 'Admin'],
        ];
        $sharedCan = Inertia::getShared('can');
        if ($sharedCan) {
            $data['can'] = array_merge_recursive($sharedCan, $data['can']);
        }

        return Inertia::render('Users/Edit', $data);
    }

    public function create(Request $request) {
        if ($request->user()->cannot('create', User::class)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }
       
        return Inertia::render('Users/Create', ['roles' => ['editor' => 'Editor', 'admin' => 'Admin']]);
    }

    public function store(Request $request) {
        if ($request->user()->cannot('create', User::class)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }

        $request->validate([
            'name' => 'required',
            'role' => 'required',
            'email' => ['required', 'email', Rule::unique('users')],
        ]);

        $create = $request->only(['name', 'email', 'role']);
        //  Mail to send user when register Added by cyblance start 
        $pass=Str::random(12);
        $create['password'] = Hash::make($pass);

        // Mail to send user when register Added by cyblance End 
        
        $user = User::create($create);

        // Mail to send user when register Added by cyblance start 
        $Userdetails = [
            'name' => $request->get('name'),
            'role' => $request->get('role'),
            'email'=> $request->get('email'),
            'password'=>$pass,
        ];
        \Mail::to($request->get('email'))->send(new \App\Mail\UserRegisterMail($Userdetails));
        // Mail to send user when register Added by cyblance end
        
        return Redirect::route('admin.users.edit', $user)->with(['info' => 'User created']);
    }


    public function update(Request $request, User $user) {
        if ($request->user()->cannot('update', $user)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }

        $request->validate([
            'name' => 'string|filled',
            'role' => 'string|filled',
            'email' => ['email', Rule::unique('users')->ignore($user->id)],
        ]);
        // update

        if ($request->has('name')) {
            $user->name = $request->input('name');
        }
        if ($request->has('email')) {
            $user->email = $request->input('email');
        }
        if ($request->has('role')) {
            if ($request->input('role') != $user->role && $request->user()->cannot('updateRole', $user)) {
                return Redirect::back()->withErrors(['error' => 'Not authorized to change role']);
            }
            $user->role = $request->input('role');
        }
        $user->save();

        return Redirect::route('admin.users.edit', $user)->with(['info' => 'User updated']);
    }

    public function resetPassword(Request $request, User $user) {
        if ($request->user()->cannot('update', $user)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }
        
        $status = Password::sendResetLink(
            ['email' => $user->email]
        );

        return $status === Password::RESET_LINK_SENT
                ? back()->with(['info' => __('Password reset link was emailed')])
                : back()->withErrors(['error' => __($status)]);  
    }

    public function destroy(Request $request, User $user) {
        if ($request->user()->cannot('delete', $user)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }
        $user->delete();
        return Redirect::route('admin.users.index')->with(['info' => 'User deleted']);
    }

    //Added By Cyblance for delete functionality
    public function bulkDelete(Request $request, User $user){
        $check=$request->all();
        if($check['getResultAction']=='trashed' || $check['getResultAction']=='restored'){
            if($check['getResultAction']=='trashed'){
                $action = date('Y-m-d H:i:s');
            }else{
                $action=Null;
            }
            User::withoutGlobalScopes()->whereIn('id', $check['getResult'])->update(['deleted_at'=> $action]);
        }else{
           User::whereIn('id', $check['getResult'])->forceDelete();
        }
        return Redirect::back()->with(['info' => 'Selected User are '.$check['getResultAction']]);
    }
}

