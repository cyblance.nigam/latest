<?php
//  Mail to send user when register Added by cyblance
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisterMail extends Mailable{
    
    use Queueable, SerializesModels;
    public $Userdetails;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Userdetails){
        
        $this->Userdetails = $Userdetails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){

        return $this->from('cyblance.yash@gmail.com')->subject('User Registration at Education International')->view('emails.userregister');
    }
}
