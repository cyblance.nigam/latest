<?php 
namespace App\Filters;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class SearchItemFilter implements Filter
{
    //Added by Cyblance search by id in listing in item
    public function __invoke(Builder $query, $value, string $property)
    {
        $locale = \App::getLocale() ?? 'en';
        
        $query->whereHas('contents', function ($q) use ($value, $locale) {
            $q->whereIn('lang', [$locale, '*'])
            //Added by Cyblance search by id in listing
                ->where('title', 'like', "%$value%")
                ->orWhere('item_id', 'like', "%$value%")
                ->orWhere('type', 'like', "%$value%")
                ->orWhere('subtype', 'like', "%$value%");
        });
    }
}