<?php 
namespace App\Filters;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class SearchLanguageFilter implements Filter{

    public function __invoke(Builder $query, $value, string $property){
        return $query->where('name', 'like', "%$value%")
        ->orWhere('id', 'like', "%$value%")
        ->orWhere('alias_name', 'like', "%$value%")
        ->orWhere('is_active', 'like', "%$value%");
    }
}
