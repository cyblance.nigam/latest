<?php 
namespace App\Filters;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class SearchCollectionFilter implements Filter{

    public function __invoke(Builder $query, $value, string $property){
        $locale = \App::getLocale() ?? 'en';
        return $query->whereHas('contents', function ($q) use ($value, $locale) {
            $q->whereIn('lang', [$locale, '*'])
                ->where('title', 'like', "%$value%")
                ->orWhere('collection_id', 'like', "%$value%")
                ->orWhere('id', 'like', "%$value%")
                ->orWhere('type', 'like', "%$value%")
                ->orWhere('layout', 'like', "%$value%");
        });
    }
}
