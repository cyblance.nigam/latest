<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Label extends Model
{
    
    //public $timestamps = false;    
    protected $table = 'labels';    

    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label',
        'is_active',        
    ];

    public function label_translates()
    {
        return $this->hasMany(LabelTranslate::class);
    }
}
